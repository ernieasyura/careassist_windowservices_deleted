﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Net.Sockets;
using System.Configuration;
using NLog;
using NLog.Config;
using QRCoder;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;

namespace WS_CareAssist_Email
{
    public class EmailManager
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private string sender, smtpHost, smtpPort, smtpcredentialUsername, smtpcredentialPassword, convertPdfPath, attachmentFile, imgEmailPath, combinePdfPath;

        public string SendEmail(n_CareAssist_WS_SendEmailListing_Result model)
        {
            MailMessage objMail = new MailMessage();
            sender = ConfigurationManager.AppSettings["sender"];
            smtpHost = ConfigurationManager.AppSettings["mailServer"];
            smtpPort = ConfigurationManager.AppSettings["mailServerPort"];
            smtpcredentialUsername = ConfigurationManager.AppSettings["username"];
            smtpcredentialPassword = ConfigurationManager.AppSettings["password"];
            convertPdfPath = ConfigurationManager.AppSettings["contentFolder"].ToString() + "Care - Assist Announcement Letter to HCPs [0].pdf";
            attachmentFile = ConfigurationManager.AppSettings["attachmentPath"];
            imgEmailPath = ConfigurationManager.AppSettings["imgEmailPath"];
            combinePdfPath = ConfigurationManager.AppSettings["contentFolder"].ToString() + "Care - Assist Announcement Letter to HCPs.pdf";
            string errorMsg="";

            try
            {
                objMail.From = new MailAddress(sender);
                string[] arrEmail = model.email.Split(';');
                foreach (string emailTo in arrEmail)
                {
                    if (emailTo != "")
                    { objMail.To.Add(new MailAddress(emailTo.Trim())); }
                }
                objMail.Subject = "CARE-ASSIST Program";
                objMail.Body = EmailTemplate(model);

                //---------------------------------------------------------------------------------
                string html = GetAttachment(model);
                //logger.Error("1");
                Byte[] pdf = PdfSharpConvert(html);
                //logger.Error("2");
                System.IO.File.WriteAllBytes(convertPdfPath, pdf);
                //logger.Error("3");

                // merge multiple pdf
                using (PdfDocument one = PdfReader.Open(convertPdfPath, PdfDocumentOpenMode.Import))
                using (PdfDocument two = PdfReader.Open(attachmentFile, PdfDocumentOpenMode.Import))
                using (PdfDocument outPdf = new PdfDocument())
                {
                    CopyPages(one, outPdf);
                    CopyPages(two, outPdf);

                    outPdf.Save(combinePdfPath);
                }

                // Create  the file attachment for this email message.
                Attachment data = new Attachment(combinePdfPath, MediaTypeNames.Application.Octet);
                // Add time stamp information for the file.
                ContentDisposition disposition = data.ContentDisposition;
                disposition.CreationDate = System.IO.File.GetCreationTime(combinePdfPath);
                disposition.ModificationDate = System.IO.File.GetLastWriteTime(combinePdfPath);
                disposition.ReadDate = System.IO.File.GetLastAccessTime(combinePdfPath);
                // Add the file attachment to this email message.
                objMail.Attachments.Add(data);
                //objMail.Attachments.Add(new Attachment(attachmentFile, MediaTypeNames.Application.Octet));
                //---------------------------------------------------------------------------------

                objMail.IsBodyHtml = true;

                SmtpClient objSMTP = new SmtpClient(smtpHost, Convert.ToInt32(smtpPort));
                objSMTP.Credentials = new NetworkCredential(smtpcredentialUsername, smtpcredentialPassword);
                objSMTP.Send(objMail);
                objMail = null;

                ContentDisposition cd = data.ContentDisposition;
                data.Dispose();
                //logger.Error("4");
                if (File.Exists(convertPdfPath))
                { File.Delete(convertPdfPath); }
                if (File.Exists(combinePdfPath))
                { File.Delete(combinePdfPath); }
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
                logger.Error("UUID :" + model.clinicUUID + " (" + ex.Message + ") ");
            }

            return errorMsg;
        }  

        private string EmailTemplate(n_CareAssist_WS_SendEmailListing_Result details)
        {
            string body = "";
            string urlLink = ConfigurationManager.AppSettings["enrolFormURL"].ToString() + details.clinicUUID.ToString();
            string templatePath = ConfigurationManager.AppSettings["templatePath"].ToString();
            string imgPath = imgEmailPath + "img-email.png";

            using (WebClient client = new WebClient())
            {
                body = client.DownloadString(templatePath);
                body = body.Replace("@urlLink", urlLink);
                body = body.Replace("@imgPath", imgPath);
            }

            return body;
        }

        private void CopyPages(PdfDocument from, PdfDocument to)
        {
            for (int i = 0; i < from.PageCount; i++)
            {
                to.AddPage(from.Pages[i]);
            }
        }

        private string GetAttachment(n_CareAssist_WS_SendEmailListing_Result details)
        {
            string html = "";
            string htmlPath = ConfigurationManager.AppSettings["letterPath"].ToString();
            string imgLogoPath = ConfigurationManager.AppSettings["contentFolder"].ToString() + "logo.png";
            string imgMSCPath = ConfigurationManager.AppSettings["contentFolder"].ToString() + "msc.png";

            using (WebClient client = new WebClient())
            {
                DateTime todayDate = DateTime.Now;
                //string dtToday = todayDate.ToString("dd MMMM yyyy");
                string suffix = "<sup>" + GetDaySuffix(todayDate.Day) + "</sup>";
                string dtToday = string.Format("{0:dd}{1} {0:MMMM yyyy}", DateTime.Now, suffix);

                html = client.DownloadString(htmlPath);
                html = html.Replace("@imgLogoPath", imgLogoPath);
                html = html.Replace("@imgMSCPath", imgMSCPath);
                html = html.Replace("@todayDate", dtToday);
                html = html.Replace("@clinicName", details.clinicName);
                html = html.Replace("@clinicAddress", details.clinicAddress);
                html = html.Replace("@contactPerson", details.contactPerson);
            }
            return html;
        }

        private static Byte[] PdfSharpConvert(String html)
        {
            Byte[] res = null;
            using (MemoryStream ms = new MemoryStream())
            {
                var pdf = TheArtOfDev.HtmlRenderer.PdfSharp.PdfGenerator.GeneratePdf(html, PdfSharp.PageSize.A4, 20);
                pdf.Save(ms);
                res = ms.ToArray();
            }
            return res;
        }

        private string GetDaySuffix(int day)
        {
            switch (day)
            {
                case 1:
                case 21:
                case 31:
                    return "st";
                case 2:
                case 22:
                    return "nd";
                case 3:
                case 23:
                    return "rd";
                default:
                    return "th";
            }
        }
    }
}
