﻿using NLog;
using System;
using System.Linq;
using System.ServiceProcess;
using System.Timers;
using System.Configuration;

namespace WS_CareAssist_Email
{
    public partial class Service1 : ServiceBase
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();        
        private System.Timers.Timer timer = new System.Timers.Timer();
       
        public Service1()
        {
            InitializeComponent();            
        }

        protected override void OnStart(string[] args)
        {
            logger.Info("Email service is started.");
            // Timer Code               
            timer.Interval = 1 * Convert.ToInt32(ConfigurationManager.AppSettings["sendEmailInXSecs"]) * 1000; //X secs run once           
            timer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
            timer.AutoReset = true;
            timer.Enabled = true;
        }

        protected override void OnStop()
        {
            logger.Info("Email service is stopped.");
            timer.Stop();
            timer.Dispose();
        }
        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {         
            try
            {
                RunHCPEnrolEmail();
            }
            catch (Exception ex)
            {
                //logger.Error(ex.Message);
                logger.Error(ex.Message);
                //logger.Info(ex.Message);
            }
        }
        private void RunHCPEnrolEmail()
        {
            n_CareAssist_WS_SendEmailListing_Result model;
            EmailManager emailManager = new EmailManager();
            string UUID = string.Empty;

            try
            {
                using (var context = new icareEntities())
                {
                    model = context.n_CareAssist_WS_SendEmailListing().FirstOrDefault();

                    if (model != null)
                    {
                        UUID = model.clinicUUID.ToString();
                        logger.Info("Sending email...");
                        string errorMsg = emailManager.SendEmail(model);
                        if (errorMsg == "")
                        {
                            logger.Info("UUID: " + model.clinicUUID + " (Email Sent) ");
                        }
                        else
                        {
                            logger.Info("UUID: " + model.clinicUUID + " (Failed Sent Email) ");
                        }
                        context.n_CareAssist_WS_UpdateEmailSent(model.clinicUUID, model.mailId);
                        context.SaveChangesAsync();
                        logger.Info("UUID: " + model.clinicUUID + " (UpdateEmailSent) ");
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("UUID: " + UUID + " (" + ex.Message + ") ");
            }
        }

    }
}
